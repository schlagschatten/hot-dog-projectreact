const Pool = require("pg").Pool

const pool = new Pool ({
    user: "postgres",
    password: "romko12345",
    host: "localhost",
    port: 5432,
    database: "test_database"
})
pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err)
    process.exit(-1)
})
module.exports = pool